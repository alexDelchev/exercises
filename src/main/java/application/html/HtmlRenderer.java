package application.html;

import org.springframework.stereotype.Component;

@Component
public class HtmlRenderer {

    public String renderAnchorTag(String targetEndpoing, String label){
        return String.format("<a href=%s>%s</a><br>", targetEndpoing, label);
    }
}
