package application.controller;

import application.html.HtmlRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class DataController {

    @Autowired
    private HtmlRenderer htmlRenderer;

    @Autowired
    private JdbcTemplate jdbcTemplate;
}
